/**
 * de.sb.broker.ClosedAuctionsController: broker preferences controller.
 * Copyright (c) 2013-2015 Sascha Baumeister
 */
"use strict";

this.de = this.de || {};
this.de.sb = this.de.sb || {};
this.de.sb.broker = this.de.sb.broker || {};
(function () {
    var SUPER = de.sb.broker.Controller;

    var QueryString = de.sb.util.QueryString;
    var TimeHelper = de.sb.util.TimeHelper;

    var statusAccumulator = new de.sb.util.StatusAccumulator();

    /**
     * Creates a new preferences controller that is derived from an abstract controller.
     * @param sessionContext {de.sb.broker.SessionContext} a session context
     */
    de.sb.broker.ClosedAuctionsController = function (sessionContext) {
        SUPER.call(this, 2, sessionContext);
    }
    de.sb.broker.ClosedAuctionsController.prototype = Object.create(SUPER.prototype);
    de.sb.broker.ClosedAuctionsController.prototype.constructor = de.sb.broker.ClosedAuctionsController;

    de.sb.broker.ClosedAuctionsController.prototype.renderAuctions = function(auctions) {
        var auctionsElement = document.querySelector("#closed-seller-auctions-template").content.cloneNode(true);
        var listContainer = auctionsElement.querySelector(".auctions tbody");
        auctions.forEach(auction => {

            var creation_time = TimeHelper.format(auction.creationTimestamp);
            var closed_time = TimeHelper.format(auction.closureTimestamp);

            var winningBid = null;
            if (auction.bids.length > 0) {
                winningBid = auction.bids.sort((bid1, bid2) => bid2.price - bid1.price)[0];
            }

            var row = document.createElement("tr");

            var winner = document.createElement("td");
            winner.textContent = (winningBid ? winningBid.bidder.alias : "None");
            if (winningBid) {
              winner.setAttribute("title", winningBid.bidderContact.email);
            }

            var begin = document.createElement("td");
            begin.textContent = creation_time;

            var end = document.createElement("td");
            end.textContent = closed_time;

            var title = document.createElement("td");
            title.textContent = auction.title;
            title.setAttribute("title", auction.description);

            var units = document.createElement("td");
            units.textContent = auction.unitCount;

            var min = document.createElement("td");
            min.textContent = auction.askingPrice;

            var winPrice = document.createElement("td");
            winPrice.textContent = (winningBid ? winningBid.price : "None");

            [winner, begin, end, title, units, min, winPrice].forEach(el => row.appendChild(el));
            listContainer.appendChild(row);
        });

        document.querySelector("main").appendChild(auctionsElement);
    }

    de.sb.broker.ClosedAuctionsController.prototype.renderBids = function(auctions) {
        var auctionsElement = document.querySelector("#closed-bidder-auctions-template").content.cloneNode(true);
        var listContainer = auctionsElement.querySelector(".auctions tbody");
        auctions.forEach(auction => {

            var creation_time = TimeHelper.format(auction.creationTimestamp);
            var closed_time = TimeHelper.format(auction.closureTimestamp);

            var winningBid = null;
            if (auction.bids.length > 0) {
                winningBid = auction.bids.sort((bid1, bid2) => bid2.price - bid1.price)[0];
            }

            var row = document.createElement("tr");

            var seller = document.createElement("td");
            seller.textContent = auction.seller.alias;
            seller.setAttribute("title", auction.seller.contact.email);

            var winner = document.createElement("td");
            winner.textContent = (winningBid ? winningBid.bidder.alias : "None")
            if (winningBid) {
              winner.setAttribute("title", winningBid.bidderContact.email);
            }

            var begin = document.createElement("td");
            begin.textContent = creation_time;

            var end = document.createElement("td");
            end.textContent = closed_time;

            var title = document.createElement("td");
            title.textContent = auction.title;
            title.setAttribute("title", auction.description);

            var units = document.createElement("td");
            units.textContent = auction.unitCount;

            var min = document.createElement("td");
            min.textContent = auction.askingPrice;

            var bid = document.createElement("td");
            bid.textContent = auction.bids.find(bid => bid.bidder.identity == this.sessionContext.user.identity).price;

            var winPrice = document.createElement("td");
            winPrice.textContent = (winningBid ? winningBid.price : "None");

            [seller, winner, begin, end, title, units, min, bid, winPrice].forEach(el => row.appendChild(el));
            listContainer.appendChild(row);
        });

        document.querySelector("main").appendChild(auctionsElement);
    }

    /**
     * Displays the associated view.
     */
    de.sb.broker.ClosedAuctionsController.prototype.display = function () {
        if (!this.sessionContext.user) return;
        SUPER.prototype.display.call(this);

        var header = {
            "Content-Type": "application/json"
        };
        var closedAuctionsParams = {
            sellerId: this.sessionContext.user.identity,
            closed: true
        };
        de.sb.util.AJAX.invoke("/services/auctions?" + QueryString.build(closedAuctionsParams), "GET", header, null, this.sessionContext, (request) => {
            statusAccumulator.offer(request.status, request.statusText);
            this.displayStatus(statusAccumulator.status, statusAccumulator.statusText);

            if (request.status == 200) {
                var auctions = JSON.parse(request.responseText);
                this.renderAuctions(auctions);
            }
        });

        var bidsParams = {
            closed: true,
            bidderId: this.sessionContext.user.identity
        };
        de.sb.util.AJAX.invoke("/services/auctions?" + QueryString.build(bidsParams), "GET", header, null, this.sessionContext, (request) => {
            statusAccumulator.offer(request.status, request.statusText);
            this.displayStatus(statusAccumulator.status, statusAccumulator.statusText);

            if (request.status == 200) {
                var auctions = JSON.parse(request.responseText);
                this.renderBids(auctions);
            }
        });
    }
} ());
