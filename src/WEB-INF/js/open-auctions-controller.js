"use strict";

this.de = this.de || {};
this.de.sb = this.de.sb || {};
this.de.sb.broker = this.de.sb.broker || {};
(function () {
	var SUPER = de.sb.broker.Controller;

	var QueryString = de.sb.util.QueryString;
    var TimeHelper = de.sb.util.TimeHelper;

	/**
	 * Creates a new open auctions controller that is derived from an abstract controller.
	 * @param sessionContext {de.sb.broker.SessionContext} a session context
	 */
	de.sb.broker.OpenAuctionsController = function (sessionContext) {
		SUPER.call(this, 1, sessionContext);
	}

	de.sb.broker.OpenAuctionsController.prototype = Object.create(SUPER.prototype);
	de.sb.broker.OpenAuctionsController.prototype.constructor = de.sb.broker.OpenAuctionsController;

	/**
	 * Displays the session user.
	 */
	de.sb.broker.OpenAuctionsController.prototype.renderAuctions = function(auctions, bids) {
        var auctionsElement = document.querySelector("#open-auctions-template").content.cloneNode(true);

        auctionsElement.querySelector('button').addEventListener('click', e => {
            this.renderNewAuctionForm();
        });

        var listContainer = auctionsElement.querySelector(".auctions tbody");
        auctions.forEach(auction => {
            var creation_time = TimeHelper.format(auction.creationTimestamp);
            var closed_time = TimeHelper.format(auction.closureTimestamp);

            var row = document.querySelector("#open-auctions-row-template").content.cloneNode(true);
            row.querySelector('.seller').textContent = auction.seller.alias;
            row.querySelector('.seller').title = `${auction.seller.fullName} (${auction.seller.contact.email})`;
            row.querySelector('.begin').textContent = creation_time;
            row.querySelector('.end').textContent = closed_time;
            row.querySelector('.title').textContent = auction.title;
            row.querySelector('.title').title = auction.description;
            row.querySelector('.units').textContent = auction.unitCount;
            row.querySelector('.min').textContent = auction.askingPrice / 100.0;

            if (auction.seller.identity == this.sessionContext.user.identity) {
                var editButton = document.querySelector("#edit-bid-button-template").content.cloneNode(true);
                editButton.querySelector('button').addEventListener('click', e => {
                    this.renderEditAuctionForm(auction);
                });
                row.querySelector('.bid').appendChild(editButton);
            }
            else {
                var bidInput = document.querySelector("#bid-value-input-template").content.cloneNode(true);

                var myBid = bids.find(bid => bid.auctionReference == auction.identity);
                if (myBid) bidInput.querySelector("input").value = myBid.price / 100.0;
                bidInput.querySelector("input").addEventListener('change', e => {
                    this.modifyBid(auction.identity, parseFloat(e.currentTarget.value) * 100);
                });

                row.querySelector('.bid').appendChild(bidInput);
            }

            listContainer.appendChild(row);
        });

        document.querySelector("main").appendChild(auctionsElement);
    }

    de.sb.broker.OpenAuctionsController.prototype.renderEditAuctionForm = function(auction) {
        var formElement = document.querySelector("#auction-form-template").content.cloneNode(true);
        var inputs = formElement.querySelectorAll("input, textarea");

        var creation_time = TimeHelper.format(auction.creationTimestamp);
        var closed_time = TimeHelper.format(auction.closureTimestamp);

        inputs[0].value = creation_time;
        inputs[1].value = closed_time;
        inputs[2].value = auction.title;
        inputs[3].value = auction.description;
        inputs[4].value = auction.unitCount;
        inputs[5].value = auction.askingPrice/100.0;

        formElement.querySelector('button').addEventListener('click', e => {
            auction.title = inputs[2].value;
            auction.description = inputs[3].value;
            auction.unitCount = inputs[4].value;
            auction.askingPrice = parseFloat(inputs[5].value) * 100;

            this.putAuction(auction);
        });

        var oldForm = document.querySelector('.auction-form');
        if (oldForm) oldForm.parentNode.removeChild(oldForm);

        document.querySelector('main').appendChild(formElement);
    }

    de.sb.broker.OpenAuctionsController.prototype.renderNewAuctionForm = function() {
        var formElement = document.querySelector("#auction-form-template").content.cloneNode(true);
        var inputs = formElement.querySelectorAll("input, textarea");

        var auction = {
            creationTimestamp: Date.now(),
            closureTimestamp: Date.now() + 1000*60*60*24*30,
            title: "",
            description: "",
            unitCount: 1,
            askingPrice: 100,
            seller: this.sessionContext.user
        }

        var creation_time = TimeHelper.format(auction.creationTimestamp);
        var closed_time = TimeHelper.format(auction.closureTimestamp);

        inputs[0].value = creation_time;
        inputs[1].value = closed_time;
        inputs[2].value = auction.title;
        inputs[3].value = auction.description;
        inputs[4].value = auction.unitCount;
        inputs[5].value = auction.askingPrice/100.0;

        formElement.querySelector('button').addEventListener('click', e => {
            auction.title = inputs[2].value;
            auction.description = inputs[3].value;
            auction.unitCount = inputs[4].value;
            auction.askingPrice = parseFloat(inputs[5].value) * 100;

            this.putAuction(auction);
        });

        var oldForm = document.querySelector('.auction-form');
        if (oldForm) oldForm.parentNode.removeChild(oldForm);

        document.querySelector('main').appendChild(formElement);
    }

    de.sb.broker.OpenAuctionsController.prototype.putAuction = function(auction) {
        var header = {
            "Content-Type": "application/json"
        };
        de.sb.util.AJAX.invoke(`/services/auctions`, "PUT", header, JSON.stringify(auction), this.sessionContext, (request) => {
            this.displayStatus(request.status, request.statusText);

            if (request.status == 200) {
                this.display();
            }
        });
    }

    de.sb.broker.OpenAuctionsController.prototype.modifyBid = function(auctionIdentity, newPrice) {
        var header = {
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = {
            bidPrice: newPrice
        };
        de.sb.util.AJAX.invoke(`/services/auctions/${auctionIdentity}/bid`, "POST", header, QueryString.build(params), this.sessionContext, (request) => {
            this.displayStatus(request.status, request.statusText);

            if (request.status != 204) {
                this.display();
            }
        });
    }

	/**
	 * Displays the associated view.
	 */
	de.sb.broker.OpenAuctionsController.prototype.display = function () {
		if (!this.sessionContext.user) return;
		SUPER.prototype.display.call(this);

        var sem = new de.sb.util.Semaphore(-1);
        var statusAccumulator = new de.sb.util.StatusAccumulator();

        var auctions = null,
            bids = null;

		var header = {
			"Content-Type": "application/json"
		};
		de.sb.util.AJAX.invoke("/services/auctions?" + QueryString.build({closed: false}), "GET", header, null, this.sessionContext, (request) => {
			statusAccumulator.offer(request.status, request.statusText);

            if (request.status == 200) {
                auctions = JSON.parse(request.responseText);
            }
            sem.release();
        });

        de.sb.util.AJAX.invoke(`/services/people/${this.sessionContext.user.identity}/bids`, "GET", header, null, this.sessionContext, (request) => {
            statusAccumulator.offer(request.status, request.statusText);

            if (request.status == 200) {
                bids = JSON.parse(request.responseText);
            }
            sem.release();
        });

        sem.acquire(() => {
            this.displayStatus(statusAccumulator.status, statusAccumulator.statusText);
            if (statusAccumulator.status == 200) this.renderAuctions(auctions, bids);
        });
	}

} ());
